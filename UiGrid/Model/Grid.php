<?php
namespace Titan\UiGrid\Model;

use Magento\Framework\Model\AbstractModel;
use Titan\UiGrid\Model\ResourceModel\Grid;

class Grida extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(Grida::class);
    }
}