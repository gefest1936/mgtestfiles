<?php

namespace Titan\UiGrid\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
     public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
     {
         $installer = $setup;
         $installer->startSetup();

        //new table script will be there ---------------------------------------------

 $table = $installer->getConnection()->newTable(
    $installer->getTable('brands_tab')
)->addColumn(
    'brand_id',
    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    null,
    ['identity' => true, 'nullable' => false, 'primary' => true],
    'Brand ID'
)->addColumn(
    'brand_name',
    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    255,
    ['nullable' => true],
    'Brand Name'
)->addColumn(
    'brand_description',
    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    '64k',
    ['nullable' => true],
    'Brand Drscription'
)->addColumn(
    'brand_logo',
    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    '64k',
    ['nullable' => true],
    'Brand Logo'
)->addColumn(
    'identifier',
    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    100,
    ['nullable' => true, 'default' => null],
    'Brand Identifier'
)->addIndex(
    $installer->getIdxName('brands_tab', ['identifier']),
    ['identifier']
)->setComment(
    'Brands Table'
);
$installer->getConnection()->createTable($table);
        
        
        //new table script end -------------------------------------------------------

        $installer->endSetup();
     }
}