<?php
namespace Titan\BrandsTest\Controller\Adminhtml\Items;

class NewAction extends \Titan\BrandsTest\Controller\Adminhtml\Items
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
