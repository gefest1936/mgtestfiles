<?php
namespace Titan\BrandsTest\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;
use Titan\BrandsTest\Block\BrandsTestView;

class View extends \Magento\Framework\App\Action\Action
{
	protected $_brandstestview;

	public function __construct(
        Context $context,
        BrandsTestView $brandstestview
    ) {
        $this->_brandstestview = $brandstestview;
        parent::__construct($context);
    }

	public function execute()
    {
    	if(!$this->_brandstestview->getSingleData()){
    		throw new NotFoundException(__('Parameter is incorrect.'));
    	}
    	
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}