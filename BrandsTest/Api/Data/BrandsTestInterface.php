<?php
namespace Titan\BrandsTest\Model\Api\Data;

interface BrandsTestInterface

{
    /**
     * @return int
     */
    public function getBrandstestId();

    /**
     * @param int $id
     * @return void
     */
    public function setBrandstestId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description);

    /**
     * @return string
     */
    public function getImage();

    /**
     * @param string $image
     * @return void
     */
    public function setImage(string $image);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return void
     */
    public function setStatus(string $status);
}