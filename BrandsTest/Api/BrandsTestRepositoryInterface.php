<?php
namespace Titan\BrandsTest\Api;

use Titan\BrandsTest\Api\Data\BrandsTestInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface PostRepositoryInterface
{
    /**
     * @param int $id
     * @return \Titan\BrandsTest\Api\Data\BrandsTestInterface
     */
    public function get(int $id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Titan\BrandsTest\Api\Data\PostSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param \Titan\BrandsTest\Api\Data\BrandsTestInterface $brands
     * @return \Titan\BrandsTest\Api\Data\BrandsTestInterface
     */
    public function save(BrandsTestInterface $brands);

    /**
     * @param \Titan\BrandsTest\Api\Data\BrandsTestInterface $brands
     * @return bool
     */
    public function delete(BrandsTestInterface $brands);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id);
}