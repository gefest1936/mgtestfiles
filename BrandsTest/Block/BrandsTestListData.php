<?php
namespace Titan\BrandsTest\Block;

use Magento\Framework\View\Element\Template\Context;
use Titan\BrandsTest\Model\BrandsTestFactory;
/**
 * BrandsTest List block
 */
class BrandsTestListData extends \Magento\Framework\View\Element\Template
{
    /**
     * @var BrandsTest
     */
    protected $_brandstest;
    public function __construct(
        Context $context,
        BrandsTestFactory $brandstest
    ) {
        $this->_brandstest = $brandstest;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Titan BrandsTest Module List Page'));
        
        if ($this->getBrandsTestCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'titan.brandstest.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15))->setShowPerPage(true)->setCollection(
                $this->getBrandsTestCollection()
            );
            $this->setChild('pager', $pager);
            $this->getBrandsTestCollection()->load();
        }
        return parent::_prepareLayout();
    }

    public function getBrandsTestCollection()
    {
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 5;

        $brandstest = $this->_brandstest->create();
        $collection = $brandstest->getCollection();
        $collection->addFieldToFilter('status','1');
        //$brandstest->setOrder('brandstest_id','ASC');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}