<?php
namespace Titan\BrandsTest\Block;

/**
 * BrandsTest content block
 */
class BrandsTest extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Titan BrandsTest Module'));
        
        return parent::_prepareLayout();
    }
}
