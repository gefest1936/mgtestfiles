<?php
namespace Titan\BrandsTest\Block;

use Magento\Framework\View\Element\Template\Context;
use Titan\BrandsTest\Model\BrandsTestFactory;
use Magento\Cms\Model\Template\FilterProvider;
/**
 * BrandsTest View block
 */
class BrandsTestView extends \Magento\Framework\View\Element\Template
{
    /**
     * @var BrandsTest
     */
    protected $_brandstest;
    public function __construct(
        Context $context,
        BrandsTestFactory $brandstest,
        FilterProvider $filterProvider
    ) {
        $this->_brandstest = $brandstest;
        $this->_filterProvider = $filterProvider;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Titan BrandsTest Module View Page'));
        
        return parent::_prepareLayout();
    }

    public function getSingleData()
    {
        $id = $this->getRequest()->getParam('id');
        $brandstest = $this->_brandstest->create();
        $singleData = $brandstest->load($id);
        if($singleData->getBrandsTestId() && $singleData->getStatus() == 1){
            return $singleData;
        }else{
            return false;
        }
    }
}