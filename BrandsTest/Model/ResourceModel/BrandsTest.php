<?php
namespace Titan\BrandsTest\Model\ResourceModel;

class BrandsTest extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('titan_brandstest', 'brandstest_id');   //here "titan_brandstest" is table name and "brandstest_id" is the primary key of custom table
    }
}