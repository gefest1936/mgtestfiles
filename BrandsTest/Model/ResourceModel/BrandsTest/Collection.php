<?php
namespace Titan\BrandsTest\Model\ResourceModel\BrandsTest;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'brandstest_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Titan\BrandsTest\Model\BrandsTest',
            'Titan\BrandsTest\Model\ResourceModel\BrandsTest'
        );
    }
}