<?php
namespace Titan\BrandsTest\Model;

use Magento\Framework\Model\AbstractModel;

class BrandsTest extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Titan\BrandsTest\Model\ResourceModel\BrandsTest');
    }
}